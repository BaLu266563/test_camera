package com.example.lab1prova

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val EDIT_PROFILE = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        gallery_button.setOnClickListener {
            val editActivityIntent = Intent(this, SecondActivity::class.java)
            startActivityForResult(editActivityIntent, EDIT_PROFILE)
        }
    }
}
